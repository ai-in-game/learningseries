extends KinematicBody2D

const SPEED = 50
const GRAVITY = 20
const JUMP_FORCE = -450

var drag = 0.4
var sprite
var on_ground = false
var velocity = Vector2(0,0)
var mov_vec = Vector2(1,0)

func _ready() -> void:
	sprite = $AnimatedSprite
	sprite.play()

func switch_direction():
	mov_vec.x *= -1
	$AnimatedSprite.flip_h = !$AnimatedSprite.flip_h
	$Looker.position.x *= -1
	
func _physics_process(delta: float) -> void:
	if self.is_on_wall() or !$Looker.is_colliding():
		switch_direction()
	
	velocity.x += mov_vec.x * SPEED - drag * velocity.x
	velocity.y += GRAVITY
	velocity = move_and_slide(velocity, Vector2.UP)
	
	



func _on_Looker_visibility_changed() -> void:
	print("vis changed")

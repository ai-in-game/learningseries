# Godot  Learning Series

## What this is
 This is a godot game engine project that I built to follow the [Godot 3 Tutorial Series](https://www.youtube.com/playlist?list=PLS9MbmO_ssyDk79j9ewONxV88fD5e_o5d) on youtube.
 As this prject was used for the purpose of learning rather than that of building, The code may not be the most elegant one you've seen. This game might not even be functional by the time I'm done with this

 My intention for this project is to be sort of a snippet library or a reference library in case I come across similar problems as in this project on my game dev journey


## Running
Simply clone this repo and open it in godot
 ```
 git clone https://gitlab.com/carnaged/godot/learningseries
 ```

## References and material used
- [Godot 3 Tutorial Series](https://www.youtube.com/playlist?list=PLS9MbmO_ssyDk79j9ewONxV88fD5e_o5d) on youtube created by [Gamesfromscratch](https://www.youtube.com/channel/UCr-5TdGkKszdbboXXsFZJTQ)
- [Dungeon Tileset](https://0x72.itch.io/dungeontileset-ii) by [0x72](https://0x72.itch.io/) on itch.io
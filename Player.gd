extends KinematicBody2D

const SPEED = 80
const GRAVITY = 20
const FLOOR = Vector2(0, -1)
const JUMP_FORCE = -450

var drag = 0.4
var sprite
var on_ground = false
var velocity = Vector2(0,0)


func _ready() -> void:
	sprite = $PlayerSprite
	sprite.play()


func _physics_process(delta: float) -> void:
	if is_on_floor():
		on_ground = true
	else:
		on_ground = false
#	if self.is_on_floor() && on_ground:
#		on_ground = false
	var mov_vec = Vector2()
	if Input.is_action_pressed("move_left"):
#		velocity.x = -SPEED
		mov_vec.x -= 1
		sprite.flip_h = true
		sprite.play("move")
	elif Input.is_action_pressed("move_right"):
		sprite.play("move")
#		velocity.x = SPEED
		mov_vec.x += 1
		sprite.flip_h = false
	else:
		sprite.play("idle")		
		
	if Input.is_action_pressed("jump"):
		if on_ground:
			velocity.y = JUMP_FORCE
	
	velocity.x += mov_vec.x * SPEED - drag * velocity.x
	velocity.y += GRAVITY
		
	velocity = move_and_slide(velocity, FLOOR)
#	print(velocity)
	

func _process(delta):
	get_parent().get_node("MainCamera").position = self.position
	

